FROM docker:stable

ENV KUBECTL=v1.17.0
ENV KIND=v0.8.1

RUN apk add --no-cache wget=1.20.3-r1 bash=5.0.17-r0 openssl=1.1.1g-r0 && \
    wget -O /usr/local/bin/kind https://github.com/kubernetes-sigs/kind/releases/download/${KIND}/kind-linux-amd64 && \
    chmod +x /usr/local/bin/kind && \
    wget -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/${KUBECTL}/bin/linux/amd64/kubectl && \
    chmod +x /usr/local/bin/kubectl && \
    wget -O get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
    chmod 700 get_helm.sh && \
    ./get_helm.sh
